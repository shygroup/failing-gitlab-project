const FAIL = Deno.env.get('FAIL')

console.log('FAIL:', FAIL);

if (FAIL) {
    Deno.exit(1);
}
